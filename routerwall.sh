#!/bin/bash
strScriptVersion="3.8"
strScriptName="routerwall"
strPWD=`pwd`


InterfaceConfig="$strScriptName.conf"
ScriptConfig="$strScriptName.conf"
HelpStatement() {
  echo -e "\\vUsage: $0 [COMMAND]"
  echo -e "Used to configure a linux based IPTABLES router/firewall for use as a primary\\r\\nentry point to a private network.\\r\\n\\r\\nMain Command Usage:\\r\\n\\tstart\\t\\tCreate tables and enable IP routing\\r\\n\\tstop\\t\\tDisable IP routing and flush tables\\r\\n\\trestart\\t\\tDisable IP routing and flush tables. Create tables and enable IP routing.\\r\\n\\tflush\\t\\tFlush tables and chains only.\\r\\n\\tdisable-routing\\tDisable packet routing only.\\r\\n\\tenable-routing\\tEnable packet routing only.\\v"
  exit 1
}

#
# Check that the bash environment is in order before proceeding
#
# Ensure that root is running the script
if [ "`id -u`" != 0 ]; then
  echo -e "ERROR: You must be root to run this!"
  exit 1
fi
# Ensure that supporting files exist
if [ ! -e $strScriptName.func ]; then
  echo -e "ERROR: The supporting file $strScriptName.func was not found in the same path!"
  exit 1
fi
# Ensure that config file exists
if [ ! -e $strScriptName.conf ]; then
  echo -e "ERROR: unable to locate $strScriptName.conf please ensure this file is present.\\r\\n"
  exit 1
fi
# Check that there is an argument defined
arg1=$1
if [ ! -n "$arg1" ]; then
  HelpStatement
fi
# Transpose capitol letters to lower case letters
arg1=`echo $arg1|tr [:upper:] [:lower:]`

#
# Configurations
#
RulesConfig=`grep -i "RulesConfig" $ScriptConfig | awk -F = '{print $2}'`
iptables=`grep -i "iptables location" $ScriptConfig | awk -F = '{print $2}'`
ipset=`grep -i "ipset location" $ScriptConfig | awk -F = '{print $2}'`
verbosity=`grep -i "verbosity" $ScriptConfig | awk -F = '{print $2}'`
external_int=`grep -i "external interface" $InterfaceConfig | awk -F = '{print $2}'`
external_net=`grep -i "external net" $InterfaceConfig | awk -F = '{print $2}'`
vpn_int=`grep -i "vpn interface" $InterfaceConfig | awk -F = '{print $2}'`
vpn_net=`grep -i "vpn net" $InterfaceConfig | awk -F = '{print $2}'`
internal_int=`grep -i "internal interface" $InterfaceConfig | awk -F = '{print $2}'`
internal_net=`grep -i "internal net" $InterfaceConfig | awk -F = '{print $2}'`
ipblacklist="RouterWallIPBlacklist"
portblacklist="RouterWallPortBlacklist"

# Ensure that Ruiles file exists
if [ ! -e $RulesConfig ]; then
  echo -e "ERROR: unable to locate $RulesConfig please ensure this file is present.\\r\\n"
  exit 1
fi
#
# Get IP address for each interface
#
ifconfig|grep $vpn_int > /dev/null
if [ "$?" == "0" ]; then
  vpn_avail="true"
  vpn_ipa="`ifconfig $vpn_int | grep 'inet addr' | awk '{print $2}' | sed -e 's/.*://'`"
else
  vpn_avail="false"
fi
external_ipa="`ifconfig $external_int | grep 'inet addr' | awk '{print $2}' | sed -e 's/.*://'`"
internal_ipa="`ifconfig $internal_int | grep 'inet addr' | awk '{print $2}' | sed -e 's/.*://'`"

#
# Source the file containing our functions
#
source $strPWD/$strScriptName.func

#
# Functions live here
#
startFirewall() {
  # Turn off the system's ability to route traffic
  if [ $verbosity -gt 1 ]; then
    echo "RouterWall-firewall: Starting Firewall">>/var/log/syslog
  fi
  if [ $verbosity -gt 2 ]; then
    echo "RouterWall-firewall: Disabling IP Routing">>/var/log/syslog
  fi
  DisableRouting
  # Adjust kernel networking parameters to be more secure
  if [ $verbosity -gt 2 ]; then
    echo "RouterWall-firewall: Making Kernel Adjustments">>/var/log/syslog
  fi
  KernelAdjustments
  # Purge rules, erase custom chains, remove custom ipset lists
  FlushTables
  # Set Default policies for builtin chains to drop and accept loopback traffic on INPUT And OUTPUT, create custom ipset lists
  if [ $verbosity -gt 2 ]; then
    echo "RouterWall-firewall: Building custom chains">>/var/log/syslog
  fi
  CustomDefaultPolicies
  BuildBlockingRules
  #Build Custom Chains
  LogAndDitchChains
  InvalidFlagChain
  ICMPChain
  # Set up build in chains
  BuildPostrouteChain
  BuildPrerouteChain
  BuildInputChain
  BuildOutputChain
  BuildForwardChain
  #Enable Routing
  if [ $verbosity -gt 2 ]; then
    echo "RouterWall-firewall: Enabling IP Routing">>/var/log/syslog
  fi
  EnableRouting
}
stopFirewall() {
  # Turn off the system's ability to route traffic
  if [ $verbosity -gt 1 ]; then
    echo "RouterWall-firewall: Stopping Firewall">>/var/log/syslog
  fi
  if [ $verbosity -gt 2 ]; then
    echo "RouterWall-firewall: Disabling IP Routing">>/var/log/syslog
  fi
  DisableRouting
  # Adjust kernel networking parameters to be more secure
  if [ $verbosity -gt 2 ]; then
    echo "RouterWall-firewall: Making Kernel Adjustments">>/var/log/syslog
  fi
  KernelAdjustments
  # Purge rules, erase custom chains, remove custom ipset lists
  if [ $verbosity -gt 2 ]; then
    echo "RouterWall-firewall: Flushing iptables">>/var/log/syslog
  fi
  FlushTables
  if [ $verbosity -gt 2 ]; then
    echo "RouterWall-firewall: Disabling traffic on external interface">>/var/log/syslog
  fi
  DestroyIPSetList
  if [ $verbosity -gt 2 ]; then
    echo "RouterWall-firewall: Destroying IPSet Lists">>/var/log/syslog
  fi
  NeuterExternalInterface
}
######################################################################################################
# Script Entry Point
######################################################################################################
case $arg1 in
  start)
    startFirewall
    ;;
  stop)
    stopFirewall
    ;;
  restart)
    stopFirewall
    startFirewall
    ;;
  flush)
    FlushTables
    ;;
  disable-routing)
    DisableRouting
    ;;
  enable-routing)
    EnableRouting
    ;;
  *)
    HelpStatement
    ;;
esac